#include "Bishop.h"

Bishop::Bishop(Board* b, char name, Point point) : Tools(b, name, point)
{
}

int Bishop::isIllegalMove(Point other) const
{
    return 0;
}

int Bishop::isEmptyPlace(Point other, Point point) const
{
    return 0;
}

int Bishop::enemyKing(Point other, Point point) const
{
    return 0;
}

int Bishop::myKing(Point other, Point point) const
{
    return 0;
}
