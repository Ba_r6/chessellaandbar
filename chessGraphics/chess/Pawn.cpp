#include "Pawn.h"

Pawn::Pawn(Board* b, char name, Point point) : Tools(b, name, point)
{
}

int Pawn::isIllegalMove(Point other) const
{
    return 0;
}

int Pawn::isEmptyPlace(Point other, Point point) const
{
    return 0;
}

int Pawn::enemyKing(Point other, Point point) const
{
    return 0;
}

int Pawn::myKing(Point other, Point point) const
{
    return 0;
}
