#include "Blank.h"

Blank::Blank(Board* b, char name, Point point) : Tools(b, name, point)
{
}

int Blank::isIllegalMove(Point other) const
{
    return 0;
}

int Blank::isEmptyPlace(Point other, Point point) const
{
    return 0;
}

int Blank::enemyKing(Point other, Point point) const
{
    return 0;
}

int Blank::myKing(Point other, Point point) const
{
    return 0;
}
