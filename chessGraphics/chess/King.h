#pragma once
#include "Tools.h"
#include "Point.h"
class King : public Tools
{
public:
	King(Board* b, char name, Point point);
	virtual int isIllegalMove(Point other) const override;
	virtual int isEmptyPlace(Point other, Point point) const override;
	virtual int enemyKing(Point other, Point point) const override;
	virtual int myKing(Point other, Point point) const override;
};

