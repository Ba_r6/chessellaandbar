#pragma once
#include "Tools.h"
#include "Point.h"

class Bishop : public Tools
{
public:
	Bishop(Board* b, char name, Point point);
	virtual int isIllegalMove(Point other) const;
	virtual int isEmptyPlace(Point other, Point point) const;
	virtual int enemyKing(Point other, Point point) const override;
	virtual int myKing(Point other, Point point) const override;
};

