#include "Queen.h"

Queen::Queen(Board* b, char name, Point point) : Tools(b, name, point)
{
}

int Queen::isIllegalMove(Point other) const
{
    return 0;
}

int Queen::isEmptyPlace(Point other, Point point) const
{
    return 0;
}

int Queen::enemyKing(Point other, Point point) const
{
    return 0;
}

int Queen::myKing(Point other, Point point) const
{
    return 0;
}
