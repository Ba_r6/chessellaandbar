#pragma once
#include <iostream>
#include <string>

using std::string;
class Point
{
public:
	Point(string place);
	void setPoint(Point other);
	Point getPoint() const;
	bool isLegalPoint(string const place) const;
	char getplace0() const;
	char getplace1() const;
	void setplace1(char ch);
	void setplace0(char ch);
	~Point();

protected:
	string _place;
};

