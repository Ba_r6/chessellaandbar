#include "Night.h"

Night::Night(Board* b, char name, Point point) : Tools(b, name, point)
{
}

int Night::isIllegalMove(Point other) const
{
	int checkEmpty = isEmptyPlace(other, this->_point);
	int isLegalMov = helper(other, this->_point);
	if (checkEmpty == SAME_SRC_DST)
	{
		return SAME_SRC_DST;
	}
	else if (checkEmpty == CURR_IN_TARGET)
	{
		return CURR_IN_TARGET;
	}
	else if (checkEmpty == NO_TOOL_IN_SRC)
	{
		return NO_TOOL_IN_SRC;
	}
	else if (myKing(other, this->_point) == CHECK_ON_ME)
	{
		return CHECK_ON_ME;
	}
	else if (!isLegalMov && !checkEmpty)
	{
		return GOOD_PLAY;
	}
	/*
		else if (other.getplace1() == (this->_point.getplace1() + 2) && (((this->_point.getplace0() + 1) == other.getplace0()) || ((this->_point.getplace0() - 1) == other.getplace0())))
	{
		return GOOD_PLAY;
	}
	else if (other.getplace1() == (this->_point.getplace1() - 1) && (((this->_point.getplace0() + 2) == other.getplace0()) || ((this->_point.getplace0() - 2) == other.getplace0())))
	{
		return GOOD_PLAY;
	}
	else if (other.getplace1() == (this->_point.getplace1() - 2) && (((this->_point.getplace0() + 1) == other.getplace0()) || ((this->_point.getplace0() - 1) == other.getplace0())))
	{
		return GOOD_PLAY;
	}*/
}

int Night::isEmptyPlace(Point other, Point point) const
{
    int isEmpty = 0;
	int sr = point.getplace1() - ONE_IN_ASCII;
	int sc = point.getplace0() - A_IN_ASCII;
	int desR = other.getplace1() - ONE_IN_ASCII;
	int desC = other.getplace0() - A_IN_ASCII;

	if (desC == sc && desR == sr)
	{
		isEmpty = SAME_SRC_DST;
	}
	else if (islower(board->_board[sr][sc]->getName()) && islower(board->_board[desR][desC]->getName()))
	{
		isEmpty = CURR_IN_TARGET;
	}
	else if (isupper(board->_board[sr][sc]->getName()) && isupper(board->_board[desR][desC]->getName()))
	{
		isEmpty = CURR_IN_TARGET;
	}
	else if (isupper(board->_board[sr][sc]->getName()) && board->_color == BLACK)
	{
		isEmpty = NO_TOOL_IN_SRC;
	}
	else if (islower(board->_board[sr][sc]->getName()) && board->_color == WHITE)
	{
		isEmpty = NO_TOOL_IN_SRC;
	}
	else if (board->_board[desR][desC]->getName() == BLANK)
	{
		isEmpty = GOOD_PLAY;
	}
	return isEmpty;
}

int Night::enemyKing(Point other, Point point) const
{
	int desR = other.getplace1() - ONE_IN_ASCII;
	int desC = other.getplace0() - A_IN_ASCII;
	int isLegalMov = helper(other, this->_point);
	if(board->_color == WHITE)
	{
		if (!isLegalMov && board->_board[desR][desC]->getName() == BLACK_KING)
		{
			return CHECK_ON_ENEMY;
		}
	}
	
}

int Night::myKing(Point other, Point point) const
{
    return 0;
}

int Night::helper(Point other, Point point) const
{
	if (other.getplace1() == (this->_point.getplace1() + 1) && (((this->_point.getplace0() + 2) == other.getplace0()) || ((this->_point.getplace0() - 2) == other.getplace0())))
	{
		return GOOD_PLAY;
	}
	else if (other.getplace1() == (this->_point.getplace1() + 2) && (((this->_point.getplace0() + 1) == other.getplace0()) || ((this->_point.getplace0() - 1) == other.getplace0())))
	{
		return GOOD_PLAY;
	}
	else if (other.getplace1() == (this->_point.getplace1() - 1) && (((this->_point.getplace0() + 2) == other.getplace0()) || ((this->_point.getplace0() - 2) == other.getplace0())))
	{
		return GOOD_PLAY;
	}
	else if (other.getplace1() == (this->_point.getplace1() - 2) && (((this->_point.getplace0() + 1) == other.getplace0()) || ((this->_point.getplace0() - 1) == other.getplace0())))
	{
		return GOOD_PLAY;
	}
}

