#include "Point.h"

Point::Point(string place)
{
	if(isLegalPoint(place))
	{
		this->_place = place;
	}
}

bool Point::isLegalPoint(const string place) const
{
	if(place[0] > 'h' || place[0] < 'a')
	{
		return false;
	}
	else if (place[1] < '1' || place[1] > '8')
	{
		return false;
	}
	else
	{
		return true;
	}
}

char Point::getplace0() const
{
	return this->_place[0];
}

char Point::getplace1() const
{
	return this->_place[1];
}

void Point::setplace0(char ch) 
{
	this->_place[0] = ch;
}

void Point::setplace1(char ch)
{
	this->_place[1] = ch;
}
Point Point::getPoint() const
{
	return _place;
}

void Point::setPoint(Point other)
{
	this->setplace0(other.getplace0());
	this->setplace1(other.getplace1());
}

Point::~Point()
{
	
}
