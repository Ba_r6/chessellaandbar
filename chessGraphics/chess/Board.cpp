#pragma once

#include "Board.h"
#include "Rook.h"
#include "King.h"
#include "Bishop.h"
#include "Blank.h"
#include "Night.h"
#include "Pawn.h"
#include "Queen.h"
Board::Board()
{
}

void Board::setColor(char color)
{
    if (isupper(color))
    {
        _color = 0;
    }
    else if (islower(color))
    {
        _color = 1;
    }
}

string Board::updateBoard()
{
    int i = 0, j = 0;
    string point = "";
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 8; j++)
        {
            _theBoard[(i * 8) + j] = _board[i][j]->getName();
        }
    }
    _theBoard[64] = char(_color);
    _theBoard[65] = NULL;
    return _theBoard;
}

string Board::setFirstBoard()
{
    string firstBoard = "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0";
 
    _theBoard = firstBoard;
    int i = 0, j = 0;

    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 8; j++)
        {
            string point = "";
            point = char(97 + j);
            point += std::to_string(i + 1);
            Point val(point);

            if (firstBoard[(i * 8) + j] == 'R') 
            {
                _board[i][j] = new Rook(this, 'r', val); // 'R' represents a black rook
            }
            else if (firstBoard[(i * 8) + j] == 'r') 
            {
                _board[i][j] = new Rook(this, 'R', val); // 'r' represents a white rook
            }
            else if (firstBoard[(i * 8) + j] == 'K') 
            {
                _board[i][j] = new King(this, 'k', val); // 'K' represents a black king
            }
            else if (firstBoard[(i * 8) + j] == 'k') 
            {
                _board[i][j] = new King(this, 'K', val); // 'k' represents a white king
            }
            else if (firstBoard[(i * 8) + j] == 'B') 
            {
                _board[i][j] = new Bishop(this, 'b', val); // 'B' represents a black bishop
            }
            else if (firstBoard[(i * 8) + j] == 'b') 
            {
                _board[i][j] = new Bishop(this, 'B', val); // 'b' represents a white bishop
            }
            else if (firstBoard[(i * 8) + j] == '#') 
            {
                _board[i][j] = new Blank(this, '#', val);
            }
            else if (firstBoard[(i * 8) + j] == 'N') 
            {
                _board[i][j] = new Night(this, 'n', val);
            }
            else if (firstBoard[(i * 8) + j] == 'n') 
            {
                _board[i][j] = new Night(this, 'N', val);
            }
            else if (firstBoard[(i * 8) + j] == 'p') 
            {
                _board[i][j] = new Pawn(this, 'P', val);
            }
            else if (firstBoard[(i * 8) + j] == 'P') 
            {
                _board[i][j] = new Pawn(this, 'p', val);
            }
            else if (firstBoard[(i * 8) + j] == 'Q') 
            {
                _board[i][j] = new Queen(this, 'q', val); 
            }
            else if (firstBoard[(i * 8) + j] == 'q') 
            {
                _board[i][j] = new Queen(this, 'Q', val); 
            }
        }
    }
    return firstBoard;
}

