#pragma once

#include <iostream>
#include <string>
#include "Point.h"
#include "Board.h"

#define CURR_IN_TARGET 3
#define CHECK_ON_ENEMY 1
#define SAME_SRC_DST 7
#define NO_TOOL_IN_SRC 2
#define CHECK_ON_ME 4
#define BLANK '#'
#define GOOD_PLAY 0
#define WHITE_ROOK 'R'
#define BLACK_ROOK 'r'
#define WHITE_KING 'K'
#define BLACK_KING 'k'
#define END_LINE 8
#define ILLEGAL_MOV 6
#define WHITE 0
#define BLACK 1
#define ONE_IN_ASCII 49
#define A_IN_ASCII 97


using std::string;
class Board;

class Tools
{
protected:

	char _name;
	Point _point;

public:
	Board* board;
	Tools(Board* b,char name, Point point);
	virtual int isIllegalMove(Point other) const = 0;
	virtual int isEmptyPlace(Point other, Point point) const = 0;
	virtual int enemyKing(Point other, Point point) const = 0;
	virtual int myKing(Point other, Point point) const = 0;

	int isEmptyPlace(string mov) const;
	char getName() const;
	void setName(char name);

	~Tools();
};