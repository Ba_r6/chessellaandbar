#pragma once
#include "Tools.h"
#include "Point.h"
class Blank : public Tools
{
public:
	Blank(Board* b, char name, Point point);
	virtual int isIllegalMove(Point other) const;
	virtual int isEmptyPlace(Point other, Point point) const;
	virtual int enemyKing(Point other, Point point) const;
	virtual int myKing(Point other, Point point) const ;
};

