/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project,
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Board.h"
#include "Rook.h"
#include "King.h"
#include "Bishop.h"
#include "Blank.h"
#include "Night.h"
#include "Pawn.h"
#include "Queen.h"

using std::cout;
using std::endl;
using std::string;


void main()
{
	srand(time_t(NULL));

	Pipe p;
	bool isConnect = p.connect();

	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return;
		}
	}


	char msgToGraphics[1024];
	int i = 0;
	Board myBoard;
	int sr = 0;
	int sc = 0;
	int dr = 0;
	int dc = 0;

	string first = myBoard.setFirstBoard();
	for (i = 0; i < 66; i++)
	{
		msgToGraphics[i] = first[i];
	}

	myBoard.setColor('A');
	cout << myBoard._board[1][0]->getName();
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		sr = msgFromGraphics[1] - 49;
		sc = msgFromGraphics[0] - 97;
		dr = msgFromGraphics[3] - 49;
		dc = msgFromGraphics[2] - 97;
		for (i = 0; i < 8; i++)
		{
			for (int k = 0; k < 8; k++)
			{
				std::cout << myBoard._board[i][k]->getName() << "  ";
			}
			std::cout << '\n';
		}
		string point = "";
		point += msgFromGraphics[2];
		point += msgFromGraphics[3];
		Point val(point);

		string result = std::to_string(myBoard._board[sr][sc]->isIllegalMove(val));
		strcpy_s(msgToGraphics, sizeof(2), result.c_str());

		if (msgToGraphics[0] == '0' || msgToGraphics[0] == '1')
		{


			if (myBoard._board[sr][sc]->getName() == 'r')
			{
				myBoard._board[dr][dc] = new Rook(&myBoard, 'r', val);
			}
			else if (myBoard._board[sr][sc]->getName() == 'R')
			{
				myBoard._board[dr][dc] = new Rook(&myBoard, 'R', val);
			}
			else if (myBoard._board[sr][sc]->getName() == 'k')
			{
				myBoard._board[dr][dc] = new King(&myBoard, 'k', val);
			}
			else if (myBoard._board[sr][sc]->getName() == 'K')
			{
				myBoard._board[dr][dc] = new King(&myBoard, 'K', val);
			}
			else if (myBoard._board[sr][sc]->getName() == 'B')
			{
				myBoard._board[dr][dc] = new Bishop(&myBoard, 'B', val);
			}
			else if (myBoard._board[sr][sc]->getName() == 'b')
			{
				myBoard._board[dr][dc] = new Bishop(&myBoard, 'b', val);
			}
			else if (myBoard._board[sr][sc]->getName() == '#')
			{
				myBoard._board[dr][dc] = new Blank(&myBoard, '#', val);
			}
			else if (myBoard._board[sr][sc]->getName() == 'N')
			{
				myBoard._board[dr][dc] = new Night(&myBoard, 'N', val);
			}
			else if (myBoard._board[sr][sc]->getName() == 'n')
			{
				myBoard._board[dr][dc] = new Night(&myBoard, 'n', val);
			}
			else if (myBoard._board[sr][sc]->getName() == 'p')
			{
				myBoard._board[dr][dc] = new Pawn(&myBoard, 'p', val);
			}
			else if (myBoard._board[sr][sc]->getName() == 'P')
			{
				myBoard._board[dr][dc] = new Pawn(&myBoard, 'P', val);
			}
			else if (myBoard._board[sr][sc]->getName() == 'Q')
			{
				myBoard._board[dr][dc] = new Queen(&myBoard, 'Q', val);
			}
			else if (myBoard._board[sr][sc]->getName() == 'q')
			{
				myBoard._board[dr][dc] = new Queen(&myBoard, 'q', val);
			}
			point = char(sc + 97);
			point += char(sr + 49);
			Point val(point);
			myBoard._board[sr][sc] = new Blank(&myBoard, '#', val);

			if (myBoard._color == 0)
			{
				myBoard.setColor('b');
			}
			else if (myBoard._color == 1)
			{
				myBoard.setColor('A');
			}
			myBoard.updateBoard();
		}

		p.sendMessageToGraphics(msgToGraphics);

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();

	}

	p.close();
}