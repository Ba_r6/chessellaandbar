#include "King.h"
#include "Board.h"


King::King(Board* b, char name, Point point) : Tools(b, name, point)
{

}
int King::isIllegalMove(Point other) const
{	
	int checkEmpty = isEmptyPlace(other, this->_point);
	if (checkEmpty == SAME_SRC_DST)
	{
		return SAME_SRC_DST;
	}
	else if (checkEmpty == CURR_IN_TARGET)
	{
		return CURR_IN_TARGET;
	}
	else if (checkEmpty == NO_TOOL_IN_SRC)
	{
		return NO_TOOL_IN_SRC;
	}
	else if (myKing(other, this->_point) == CHECK_ON_ME)
	{
		return CHECK_ON_ME;
	}
	else if ((other.getplace0() == this->_point.getplace0() - 1) && (other.getplace1() == this->_point.getplace1() + 1) && !checkEmpty)
	{
		return GOOD_PLAY;
	}
	else if ((other.getplace0() == this->_point.getplace0()) && (other.getplace1() == this->_point.getplace1() + 1) && !checkEmpty)
	{
		return GOOD_PLAY;
	}
	else if ((other.getplace0() == this->_point.getplace0() + 1) && (other.getplace1() == this->_point.getplace1() + 1) && !checkEmpty)
	{
		return GOOD_PLAY;
	}
	else if ((other.getplace0() == this->_point.getplace0() + 1) && (other.getplace1() == this->_point.getplace1()) && !checkEmpty)
	{
		return GOOD_PLAY;
	}
	else if ((other.getplace0() == this->_point.getplace0() - 1) && (other.getplace1() == this->_point.getplace1()) && !checkEmpty)
	{
		return GOOD_PLAY;
	}
	else if ((other.getplace0() == this->_point.getplace0() - 1) && (other.getplace1() == this->_point.getplace1() - 1) && !checkEmpty)
	{
		return GOOD_PLAY;
	}
	else if ((other.getplace0() == this->_point.getplace0()) && (other.getplace1() == this->_point.getplace1() - 1) && !checkEmpty)
	{
		return GOOD_PLAY;
	}
	else if ((other.getplace0() == this->_point.getplace0() + 1) && (other.getplace1() == this->_point.getplace1() - 1) && !checkEmpty)
	{
		return GOOD_PLAY;
	}
	return ILLEGAL_MOV;
}

int King::isEmptyPlace(Point other, Point point) const
{
	int isEmpty = 0;
	int sr = point.getplace1() - ONE_IN_ASCII;
	int sc = point.getplace0() - A_IN_ASCII;
	int desR = other.getplace1() - ONE_IN_ASCII;
	int desC = other.getplace0() - A_IN_ASCII;

	char ch = board->_board[desR][desC]->getName();
	if (desC == sc && desR == sr)
	{
		isEmpty = SAME_SRC_DST;
	}
	else if (islower(board->_board[sr][sc]->getName()) && islower(board->_board[desR][desC]->getName()))
	{
		isEmpty = CURR_IN_TARGET;
	}
	else if (isupper(board->_board[sr][sc]->getName()) && isupper(board->_board[desR][desC]->getName()))
	{
		isEmpty = CURR_IN_TARGET;
	}
	else if (isupper(board->_board[sr][sc]->getName()) && board->_color == BLACK)
	{
		isEmpty = NO_TOOL_IN_SRC;
	}
	else if (islower(board->_board[sr][sc]->getName()) && board->_color == WHITE)
	{
		isEmpty = NO_TOOL_IN_SRC;
	}
	else if (board->_board[desR][desC]->getName() == BLANK)
	{
		isEmpty = GOOD_PLAY;
	}
	return isEmpty;
}

int King::enemyKing(Point other, Point point) const
{
	int i = 0, j = 0;
	string pointVal = "";
	Point val(point);

	if (board->_color == WHITE)
	{
		for (i = 0; i < END_LINE; i++)
		{
			for (j = 0; j < END_LINE; j++)
			{
				string point_other = "";
				point_other += char(j + 97);
				point_other += char(i + 1);
				Point valTWO(point_other);
				if (board->_board[i][j]->getName() == BLACK_ROOK && !isEmptyPlace(val, valTWO))
				{
					return CHECK_ON_ENEMY;
				}
			}
		}
	}
	else
	{
		for (i = 0; i < END_LINE; i++)
		{
			for (j = 0; j < END_LINE; j++)
			{
				string point_other = "";
				point_other += char(j + 97);
				point_other += char(i + 1);
				Point valTWO(point_other);
				if (board->_board[i][j]->getName() == WHITE_ROOK && !isEmptyPlace(val, valTWO))
				{
					return CHECK_ON_ENEMY;
				}
			}
		}
	}
}

int King::myKing(Point other, Point point) const
{
	int i = 0, j = 0;
	string pointVal = "";
	int x = other.getplace0() - A_IN_ASCII;
	int y = other.getplace1() - ONE_IN_ASCII;
	if (board->_color == WHITE)
	{
		for (i = 0; i < END_LINE; i++)
		{
			if (board->_board[x][i]->getName() == BLACK_ROOK)
			{
				return CHECK_ON_ME;
			}
		}
		for (i = 0; i < END_LINE; i++)
		{
			if (board->_board[i][y]->getName() == BLACK_ROOK)
			{
				return CHECK_ON_ME;
			}
		}
	}
	else
	{
		for (i = 0; i < END_LINE; i++)
		{
			if (board->_board[other.getplace0()][i]->getName() == WHITE_ROOK)
			{
				return CHECK_ON_ME;
			}
		}
		for (i = 0; i < END_LINE; i++)
		{
			if (board->_board[i][other.getplace1()]->getName() == WHITE_ROOK)
			{
				return CHECK_ON_ME;
			}
		}
	}
}