#pragma once
#include "Tools.h"
#include "Point.h"

class Queen : public Tools
{
public:
	Queen(Board* b, char name, Point point);
	virtual int isIllegalMove(Point other) const;
	virtual int isEmptyPlace(Point other, Point point) const;
	virtual int enemyKing(Point other, Point point) const override;
	virtual int myKing(Point other, Point point) const override;
};

