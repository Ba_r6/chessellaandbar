#pragma once
#include <iostream>
#include <string>
#include "Tools.h"

class Tools;

class Board
{
public:
	Board();
	Tools* _board[8][8];
	string _theBoard;
	int _color;
	void setColor(char color);
	string updateBoard();
	string setFirstBoard();
};

