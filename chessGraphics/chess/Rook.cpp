#include "Rook.h"
#include "Tools.h"

Rook::Rook(Board* b, char name, Point point) : Tools(b, name, point)
{
}

int Rook::isIllegalMove(Point other) const
{
	int code = ILLEGAL_MOV;
	if (other.getplace0() == this->_point.getplace0() || (other.getplace1() == this->_point.getplace1()))
	{
		code = isEmptyPlace(other, this->_point);
	}
	return code;
}

int Rook::isEmptyPlace(Point other, Point point) const
{
	int isEmpty = 0;
	int sr = point.getplace1() - ONE_IN_ASCII;
	int sc = point.getplace0() - A_IN_ASCII;
	int desR = other.getplace1() - ONE_IN_ASCII;
	int desC = other.getplace0() - A_IN_ASCII;
	if (sr == desR && sc == desC)
	{
		isEmpty = SAME_SRC_DST; //Source and destination in the same place
	}
	else if (enemyKing(other, point) == CHECK_ON_ENEMY)
	{
		isEmpty = CHECK_ON_ENEMY;
	}
	else if (myKing(other, point) == CHECK_ON_ME)
	{
		isEmpty = CHECK_ON_ME;
	}
	else if (islower(board->_board[sr][sc]->getName()) && islower(board->_board[desR][desC]->getName()))
	{
		isEmpty = CURR_IN_TARGET;
	}
	else if (isupper(board->_board[sr][sc]->getName()) && isupper(board->_board[desR][desC]->getName()))
	{
		isEmpty = CURR_IN_TARGET;
	}
	else if (isupper(board->_board[sr][sc]->getName()) && board->_color == BLACK)
	{
		isEmpty = NO_TOOL_IN_SRC;
	}
	else if (islower(board->_board[sr][sc]->getName()) && board->_color == WHITE)
	{
		isEmpty = NO_TOOL_IN_SRC;
	}
	else if (desC == sc)
	{
		if (sr > desR)
		{
			for (int i = sr; i < desR; i--)
			{
				if (board->_board[i][sc]->getName() != BLANK)
				{
					isEmpty = ILLEGAL_MOV;
				}
			}
		}
		else
		{
			for (int i = sr; i < desR; i++)
			{
				if (board->_board[i][sc]->getName() != BLANK)
				{
					isEmpty = ILLEGAL_MOV;
				}
			}
		}
	}
	else if (desR == sr)
	{
		if (desC > sc)
		{
			for (int i = sc; i < desC; i--)
			{
				if (board->_board[sr][i]->getName() != BLANK)
				{
					isEmpty = 6;
				}
			}
		}
		else
		{
			for (int i = sc; i < desC; i++)
			{
				if (board->_board[i][sc]->getName() != BLANK)
				{
					isEmpty = ILLEGAL_MOV;
				}
			}
		}
	}
	return isEmpty;
}
int Rook::enemyKing(Point other, Point point) const
{
	int i = 0;
	int sr = point.getplace0() - A_IN_ASCII;
	int sc = point.getplace1() - ONE_IN_ASCII;
	int desR = other.getplace0() - A_IN_ASCII;
	int desC = other.getplace1() - ONE_IN_ASCII;
	//int isEmpty = isEmptyPlace(other, point);
	if (sr == desR)
	{
		for (i = 0; i < END_LINE; i++)
		{
			if ((board->_board[i][sc]->getName() == BLACK_KING && board->_color == WHITE) )
			{
				return CHECK_ON_ENEMY;
			}
			else if ((board->_board[i][sc]->getName() == WHITE_KING && board->_color != WHITE ))
			{
				return CHECK_ON_ENEMY;
			}
		}

	}
	else
	{
		for (i = 0; i < END_LINE; i++)
		{
			if ((board->_board[sr][i]->getName() == BLACK_KING && board->_color == WHITE))
			{
				return 1;
			}
			else if ((board->_board[sr][i]->getName() == WHITE_KING && board->_color != WHITE))
			{
				return 1;
			}
		}
	}
}

int Rook::myKing(Point other, Point point) const
{
	int i = 0;
	int sr = point.getplace0() - A_IN_ASCII;
	int sc = point.getplace1() - ONE_IN_ASCII;
	int desR = other.getplace0() - A_IN_ASCII;
	int desC = other.getplace1() - ONE_IN_ASCII;
	//int isEmpty = isEmptyPlace(other, point);
	if (sr == desR)
	{
		for (i = 0; i < END_LINE; i++)
		{
			if ((board->_board[i][sc]->getName() == BLACK_KING && board->_color == WHITE))
			{
				return CHECK_ON_ME;
			}
			else if ((board->_board[i][sc]->getName() == WHITE_KING && board->_color != WHITE))
			{
				return CHECK_ON_ME;
			}
		}

	}
	else
	{
		for (i = 0; i < END_LINE; i++)
		{
			if ((board->_board[sr][i]->getName() == BLACK_KING && board->_color == WHITE))
			{
				return CHECK_ON_ME;
			}
			else if ((board->_board[sr][i]->getName() == WHITE_KING && board->_color != WHITE))
			{
				return CHECK_ON_ME;
			}
		}
	}
}

