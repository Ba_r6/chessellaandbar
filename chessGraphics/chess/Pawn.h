#pragma once

#include "Tools.h"
#include "Point.h"
#include "Board.h"

class Pawn : public Tools
{
public:
	Pawn(Board* b, char name, Point point);
	virtual int isIllegalMove(Point other) const;
	virtual int isEmptyPlace(Point other, Point point) const;
	virtual int enemyKing(Point other, Point point) const override;
	virtual int myKing(Point other, Point point) const override;
};

